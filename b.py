def jam(a, b):
    return a + b
    
def zarb_positives(a, b):
    if int(a) != a or int(b) != b:
        return None
    if a < 0 or b < 0:
        return None
    return a * b
