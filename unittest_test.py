import unittest
from b import *


class TestSum(unittest.TestCase):

    def test_sum(self):
        self.assertEqual(jam(1,2), 3, "Should be 3")

    def test_zarb1(self):
        self.assertEqual(zarb_positives(1.5, 2), None, "Should be None")
        
    def test_zarb2(self):
        self.assertEqual(zarb_positives(5, -2), None, "Should be None")
        
    def test_zarb3(self):
        self.assertEqual(zarb_positives(3, 2), 6, "Should be 6")

if __name__ == '__main__':
    unittest.main()